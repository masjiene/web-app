# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [2.3.0](https://bitbucket.org/masjiene/web-app/compare/v2.2.0...v2.3.0) (2019-03-10)


### Bug Fixes

* **self:** always forget some stuff, please use PR from now on. ([126b9b8](https://bitbucket.org/masjiene/web-app/commits/126b9b8))
* **self:** another possible fix ([8802643](https://bitbucket.org/masjiene/web-app/commits/8802643))
* **self:** broken ([e902bb6](https://bitbucket.org/masjiene/web-app/commits/e902bb6))
* **self:** create house bug fixed ([df31a67](https://bitbucket.org/masjiene/web-app/commits/df31a67))
* **self:** damn, broke it again ([4abf6da](https://bitbucket.org/masjiene/web-app/commits/4abf6da))
* **self:** forgot to revert settings ([60ed01d](https://bitbucket.org/masjiene/web-app/commits/60ed01d))
* **self:** refactored build fixws ([f4d048f](https://bitbucket.org/masjiene/web-app/commits/f4d048f))
* **self:** removed console logs ([9446c4a](https://bitbucket.org/masjiene/web-app/commits/9446c4a))
* **self:** removed websockets ([cc9c13b](https://bitbucket.org/masjiene/web-app/commits/cc9c13b))
* **self:** websockets connection check ([6ca2749](https://bitbucket.org/masjiene/web-app/commits/6ca2749))
* **self:** Websockets finally working ([5b292a1](https://bitbucket.org/masjiene/web-app/commits/5b292a1))
* **self:** ws fixes ([fe7eb2b](https://bitbucket.org/masjiene/web-app/commits/fe7eb2b))
* **self:** WS WORKS!!! ([c698200](https://bitbucket.org/masjiene/web-app/commits/c698200))
* **self:** wss fix ([af94b4f](https://bitbucket.org/masjiene/web-app/commits/af94b4f))
* **slef:** fixing out of sync issue ([2574e68](https://bitbucket.org/masjiene/web-app/commits/2574e68))


### Features

* **self:** added vibrate :D ([e727edb](https://bitbucket.org/masjiene/web-app/commits/e727edb))
* **self:** check api test, eventListener devicelight ([25d604c](https://bitbucket.org/masjiene/web-app/commits/25d604c))
* **self:** final touches on ws ([3016b72](https://bitbucket.org/masjiene/web-app/commits/3016b72))
* **self:** Removed websockets and added grapgql returns ([61b4409](https://bitbucket.org/masjiene/web-app/commits/61b4409))
* **self:** websockets and navigator.vibrate ([81ae436](https://bitbucket.org/masjiene/web-app/commits/81ae436))



<a name="2.2.0"></a>
# [2.2.0](https://bitbucket.org/masjiene/web-app/compare/v2.1.0...v2.2.0) (2019-02-28)


### Bug Fixes

* **all:** api url is wrong again ([b52e7b4](https://bitbucket.org/masjiene/web-app/commits/b52e7b4))
* **all:** changed more cookie settings ([33115c6](https://bitbucket.org/masjiene/web-app/commits/33115c6))
* **all:** login refresh bug ([0792d54](https://bitbucket.org/masjiene/web-app/commits/0792d54))
* **all:** webpack publicpath broke shit ([0404dcb](https://bitbucket.org/masjiene/web-app/commits/0404dcb))
* **self:** bug fix final attempt ([5edc376](https://bitbucket.org/masjiene/web-app/commits/5edc376))
* **self:** bug fix first attempt ([eb8b93e](https://bitbucket.org/masjiene/web-app/commits/eb8b93e))
* **self:** bug fix fourth attempt ([7254bcc](https://bitbucket.org/masjiene/web-app/commits/7254bcc))
* **self:** bug fix second attempt ([105946e](https://bitbucket.org/masjiene/web-app/commits/105946e))
* **self:** bug fix third attempt ([cb17949](https://bitbucket.org/masjiene/web-app/commits/cb17949))
* **self:** change something in the api ([2709777](https://bitbucket.org/masjiene/web-app/commits/2709777))
* **self:** fixed login bug ([66d9e5e](https://bitbucket.org/masjiene/web-app/commits/66d9e5e))
* **self:** logout bug fix ([79ca1b4](https://bitbucket.org/masjiene/web-app/commits/79ca1b4))
* **self:** missed console.log ([1839017](https://bitbucket.org/masjiene/web-app/commits/1839017))
* **self:** removed unused package ([09043e3](https://bitbucket.org/masjiene/web-app/commits/09043e3))
* **self:** wrong api url ([5042784](https://bitbucket.org/masjiene/web-app/commits/5042784))


### Features

* **all:** change cookie settings ([49c6caf](https://bitbucket.org/masjiene/web-app/commits/49c6caf))
* **all:** changed routing, auth and a few other things ([0174e6e](https://bitbucket.org/masjiene/web-app/commits/0174e6e))
* **all:** logout functionality added ([64abb01](https://bitbucket.org/masjiene/web-app/commits/64abb01))
* **self:** added logo to login page ([d837bdf](https://bitbucket.org/masjiene/web-app/commits/d837bdf))
* **self:** added more subscription thinks and style create house ([3b6f455](https://bitbucket.org/masjiene/web-app/commits/3b6f455))
* **self:** added production type builds ([bba6716](https://bitbucket.org/masjiene/web-app/commits/bba6716))
* **self:** added signup options ([198c6cc](https://bitbucket.org/masjiene/web-app/commits/198c6cc))
* **self:** added source maps ([f8a60c6](https://bitbucket.org/masjiene/web-app/commits/f8a60c6))
* **self:** added toasts for success and error and added create home form and ability ([d2cd1b9](https://bitbucket.org/masjiene/web-app/commits/d2cd1b9))
* **self:** added websockets to update state ([030526b](https://bitbucket.org/masjiene/web-app/commits/030526b))
* **self:** adding graphql mutations ([357eb08](https://bitbucket.org/masjiene/web-app/commits/357eb08))
* **self:** almost done with graphql mutatiooons ([778608c](https://bitbucket.org/masjiene/web-app/commits/778608c))
* **self:** build fix ([ea5ba5c](https://bitbucket.org/masjiene/web-app/commits/ea5ba5c))
* **self:** change dist cleanup ([b640244](https://bitbucket.org/masjiene/web-app/commits/b640244))
* **self:** changes to webpack local webpack dev server ([2d02585](https://bitbucket.org/masjiene/web-app/commits/2d02585))
* **self:** cleanup ([1bf7376](https://bitbucket.org/masjiene/web-app/commits/1bf7376))
* **self:** eslint for webpack ([691a137](https://bitbucket.org/masjiene/web-app/commits/691a137))
* **self:** final bit of room list ([5c79ea7](https://bitbucket.org/masjiene/web-app/commits/5c79ea7))
* **self:** fixed all linting issues ([2bb761b](https://bitbucket.org/masjiene/web-app/commits/2bb761b))
* **self:** graphql subscriptions working! ([5095310](https://bitbucket.org/masjiene/web-app/commits/5095310))
* **self:** icon imports optimized ([f412c07](https://bitbucket.org/masjiene/web-app/commits/f412c07))
* **self:** major changes to frontend flows ([5576d34](https://bitbucket.org/masjiene/web-app/commits/5576d34))
* **self:** more graphql sub code ([f075e2b](https://bitbucket.org/masjiene/web-app/commits/f075e2b))
* **self:** new image ([9e065d6](https://bitbucket.org/masjiene/web-app/commits/9e065d6))
* **self:** new login page styling ([64d0f52](https://bitbucket.org/masjiene/web-app/commits/64d0f52))
* **self:** new new logo! hahaha ([b138902](https://bitbucket.org/masjiene/web-app/commits/b138902))
* **self:** random pimping of components ([8916b7b](https://bitbucket.org/masjiene/web-app/commits/8916b7b))
* **self:** refactored login styling for responsiveness ([5486fdf](https://bitbucket.org/masjiene/web-app/commits/5486fdf))
* **self:** removed clean dir from run ([e5d0f18](https://bitbucket.org/masjiene/web-app/commits/e5d0f18))
* **self:** toasts styling ([dc95e89](https://bitbucket.org/masjiene/web-app/commits/dc95e89))



<a name="2.1.0"></a>
# [2.1.0](https://bitbucket.org/masjiene/web-app/compare/v2.0.0...v2.1.0) (2019-02-07)


### Bug Fixes

* **web:** removed jshint for eslint ([98c9381](https://bitbucket.org/masjiene/web-app/commits/98c9381))
* **web app:** added basic styling, mad a bit of a mess, but stuff still works ([f70d946](https://bitbucket.org/masjiene/web-app/commits/f70d946))
* **web app:** added sourcemaps and configured .prettier.rc ([bf3ffd3](https://bitbucket.org/masjiene/web-app/commits/bf3ffd3))
* **web app:** aws deployment test ([49af81b](https://bitbucket.org/masjiene/web-app/commits/49af81b))
* **web app:** cleanup ([fa3568c](https://bitbucket.org/masjiene/web-app/commits/fa3568c))
* **web app:** fixed bitbucket pipeline yaml indentation ([e1e6c69](https://bitbucket.org/masjiene/web-app/commits/e1e6c69))
* **web app:** fixed noscript contect ([e7241d3](https://bitbucket.org/masjiene/web-app/commits/e7241d3))
* **web app:** mock changed, probably broken ([101a00e](https://bitbucket.org/masjiene/web-app/commits/101a00e))
* **web-app:** Added new components and restructured the project ([bb034e8](https://bitbucket.org/masjiene/web-app/commits/bb034e8))
* **web-app:** docker changes and packages cleanup ([e50d890](https://bitbucket.org/masjiene/web-app/commits/e50d890))
* **whole app:** added the live port ([0078ebd](https://bitbucket.org/masjiene/web-app/commits/0078ebd))
* **whole project:** Removed the sending of the userid to the server ([a89bb7c](https://bitbucket.org/masjiene/web-app/commits/a89bb7c))


### Features

* **self:** added loaders and styling for framework pages ([d650518](https://bitbucket.org/masjiene/web-app/commits/d650518))
* **self:** added loads of new styling ([258a380](https://bitbucket.org/masjiene/web-app/commits/258a380))
* **self:** added more webpack config, port settings and such ([768dacc](https://bitbucket.org/masjiene/web-app/commits/768dacc))
* **self:** added new loading mech and errors ([12a24a9](https://bitbucket.org/masjiene/web-app/commits/12a24a9))
* **self:** Added new styling for login page ([39bd952](https://bitbucket.org/masjiene/web-app/commits/39bd952))
* **self:** background image and room and device styling ([fb62b04](https://bitbucket.org/masjiene/web-app/commits/fb62b04))
* **self:** finalized some styling ([57d98d7](https://bitbucket.org/masjiene/web-app/commits/57d98d7))
* **self:** fixed webpack warnings and added checks ([c2a1f35](https://bitbucket.org/masjiene/web-app/commits/c2a1f35))
* **self:** more scope creep ([e4388ed](https://bitbucket.org/masjiene/web-app/commits/e4388ed))
* **slf:** styling changes i think, lost track ([0428682](https://bitbucket.org/masjiene/web-app/commits/0428682))
* **web app:** added aws deployments ([dbd1f24](https://bitbucket.org/masjiene/web-app/commits/dbd1f24))
* **web app:** Added custom routing for app ([ce86607](https://bitbucket.org/masjiene/web-app/commits/ce86607))
* **web app:** added homepage, rooms and devices ([e0cad30](https://bitbucket.org/masjiene/web-app/commits/e0cad30))
* **web app:** added loader for house api call ([659b5ba](https://bitbucket.org/masjiene/web-app/commits/659b5ba))
* **web app:** Added new components ([e85e8e9](https://bitbucket.org/masjiene/web-app/commits/e85e8e9))
* **web app:** Added new graphql mock itmes ([fa2d940](https://bitbucket.org/masjiene/web-app/commits/fa2d940))
* **web app:** added production build, dev build and config for local run ([ca2b8ec](https://bitbucket.org/masjiene/web-app/commits/ca2b8ec))
* **web app:** added scroll snap for paging rooms ([235a85c](https://bitbucket.org/masjiene/web-app/commits/235a85c))
* **web app:** added service worker generations in webpack ([3a65f2f](https://bitbucket.org/masjiene/web-app/commits/3a65f2f))
* **web app:** adding routing, but seem to have wondered off ([9d896cd](https://bitbucket.org/masjiene/web-app/commits/9d896cd))
* **web app:** busy adding auth gurads ([f28d89a](https://bitbucket.org/masjiene/web-app/commits/f28d89a))
* **web app:** general styling for mobiel and esktop ([3826918](https://bitbucket.org/masjiene/web-app/commits/3826918))
* **web app:** login and devices styled ([82a4bc9](https://bitbucket.org/masjiene/web-app/commits/82a4bc9))
* **web app:** Login hooked up to graphql login endpoint ([b95ff78](https://bitbucket.org/masjiene/web-app/commits/b95ff78))
* **web app:** Major changes, more than just styling ([57d2ee5](https://bitbucket.org/masjiene/web-app/commits/57d2ee5))
* **web app:** Major refactor of redux and services ([eacf367](https://bitbucket.org/masjiene/web-app/commits/eacf367))
* **web app:** more reducers and graphql added ([111f255](https://bitbucket.org/masjiene/web-app/commits/111f255))
* **web app:** scroll snap try ([cb19fe9](https://bitbucket.org/masjiene/web-app/commits/cb19fe9))
* **web app:** service working start_url changed ([be9e3d8](https://bitbucket.org/masjiene/web-app/commits/be9e3d8))
* **web app:** styled footer component ([93304a6](https://bitbucket.org/masjiene/web-app/commits/93304a6))
* **web app:** styling for login page ([2181449](https://bitbucket.org/masjiene/web-app/commits/2181449))
* **web app:** styling rooms to look like something ([b204550](https://bitbucket.org/masjiene/web-app/commits/b204550))
* **Web app:** Added the ability to mock graphql ([2f39114](https://bitbucket.org/masjiene/web-app/commits/2f39114))
* **web server:** added error handling ([949aa8f](https://bitbucket.org/masjiene/web-app/commits/949aa8f))
* **web-app:** Adding router for web components ([744bcfa](https://bitbucket.org/masjiene/web-app/commits/744bcfa))
* **whole web app:** major changes to components layout and eslinting setup ([3dbaccd](https://bitbucket.org/masjiene/web-app/commits/3dbaccd))


### Performance Improvements

* **web app:** Added https and ssl capabilities to dockerfile ([12c6aef](https://bitbucket.org/masjiene/web-app/commits/12c6aef))



<a name="2.0.0"></a>
# 2.0.0 (2018-12-16)


### Bug Fixes

* **App:** Build changes ([a7c0e7d](https://bitbucket.org/masjiene/web-app/commits/a7c0e7d))
* **App:** Creating more structure under components ([6d66531](https://bitbucket.org/masjiene/web-app/commits/6d66531))
* **data-server:** performance checkup ([db77d59](https://bitbucket.org/masjiene/web-app/commits/db77d59))
* **New stuff:** I addded new stuff ([67c6fc2](https://bitbucket.org/masjiene/web-app/commits/67c6fc2))
* **Project:** Added changelogs ([57cae52](https://bitbucket.org/masjiene/web-app/commits/57cae52))
* **Project:** Added docker file and some state management issues ([6d27ca5](https://bitbucket.org/masjiene/web-app/commits/6d27ca5))
* **Project:** Changed way of working an folder structure ([cf42e45](https://bitbucket.org/masjiene/web-app/commits/cf42e45))
* **Project:** Fixing the build ([38585b9](https://bitbucket.org/masjiene/web-app/commits/38585b9))
* **Project:** Getting unit tests set up ([b78d2dc](https://bitbucket.org/masjiene/web-app/commits/b78d2dc))
* **Project:** removed docker-compose file again ([00222e1](https://bitbucket.org/masjiene/web-app/commits/00222e1))
* **Project:** Removed lit-html using plan web components ([0c3f455](https://bitbucket.org/masjiene/web-app/commits/0c3f455))
* **Web:** Fiddled with WepPack config things, might be a bit dirty now ([947e2a6](https://bitbucket.org/masjiene/web-app/commits/947e2a6))
* **web app:** Build and deps config ([6c52046](https://bitbucket.org/masjiene/web-app/commits/6c52046))
* **web app:** Service workers added ([ac1fe20](https://bitbucket.org/masjiene/web-app/commits/ac1fe20))


### Chores

* **Project:** Busy setting up the project ([c91e5eb](https://bitbucket.org/masjiene/web-app/commits/c91e5eb))


### Features

* **App:** Created render class and refactored webpack.config ([ad54b27](https://bitbucket.org/masjiene/web-app/commits/ad54b27))
* **Project:** Added a whole lot of new stuff and broken stuff ([f3ce856](https://bitbucket.org/masjiene/web-app/commits/f3ce856))
* **Project:** Adding statemanagement ([8a46497](https://bitbucket.org/masjiene/web-app/commits/8a46497))
* **State mangement, pwa:** Added redux and pwa manifest ([8bcb700](https://bitbucket.org/masjiene/web-app/commits/8bcb700))


### BREAKING CHANGES

* **Project:** Setup
