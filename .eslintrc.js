module.exports = {
	parser: 'babel-eslint',
	env: {
		browser: true,
		es6: true
	},
	extends: 'eslint:recommended',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		allowImportExportEverywhere: true
	},
	rules: {
		allowTemplateLiterals: true,
		'constructor-super': ['error'],
		indent: ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		quotes: ['error', 'single'],
		semi: ['error', 'always'],
		'template-curly-spacing': ['error', 'never'],
		'no-console': 'off',
		'experimentalDecorators': true
	}
};
