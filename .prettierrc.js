module.exports = {
	trailingComma: 'es5',
	bracketSpacing: true,
	arrowParens: always,
	tabWidth: 4,
	semi: false,
	singleQuote: true,
}
