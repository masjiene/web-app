const CleanWebpackPlugin = require('clean-webpack-plugin');
var FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const path = require('path');

module.exports = {
	target: 'web',
	entry: {
		app: `${path.resolve(__dirname, 'src')}/index.js`,
	},
	output: {
		filename: '[name].bundle.js',
		chunkFilename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	resolve: {
		extensions: ['.js'],
	},
	optimization: {
		usedExports: true,
		splitChunks: {
			chunks: 'all',
		},
	},
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'eslint-loader',
				options: {
					formatter: require('eslint/lib/formatters/stylish'),
				}
			},
			{
				test: /\.js$/,
				exclude: [/node_modules/, /test/, /dist/],
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.html$/,
				exclude: [/node_modules/, /test/, /dist/],
				include: [
					`${path.resolve(__dirname, 'src')}/index.html`
				],
				loader: 'html-loader'
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				use: [
					'file-loader?name=[name].[ext]',
					{
						loader: 'image-webpack-loader',
						options: {
							bypassOnDebug: true,
							disable: false,
						},
					},
				],
			},
		],
	},
	stats: {
		colors: true,
	},
	plugins: [
		new CleanWebpackPlugin({
			cleanOnceBeforeBuildPatterns: ['dist/*']
		}),
		new FriendlyErrorsWebpackPlugin(),
		new WebappWebpackPlugin({
			logo: './images/logo.png',
			favicons: {
				appName: 'swyser.app',
				developerURL: 'swyser.app',
				appDescription: 'swyser.app',
				developerName: 'Francois van der Merwe (swyserdev.co.za)',
				background: '#ddd',
				theme_color: '#272727',
				lang: 'en-US',
				appleStatusBarStyle: 'black-translucent',
				display: 'standalone',
				start_url: '/',
				logging: false,
				version: '1.0.0',
				icons: {
					android: true,
					appleIcon: true,
					appleStartup: true,
					coast: false,
					favicons: true,
					firefox: true,
					windows: true,
					yandex: false,
				},
			},
		}),
		new HtmlWebPackPlugin({
			filename: 'index.html',
			title: 'swyser.app',
			minify: true,
			template: './src/index.html',
		}),
		new WorkboxPlugin.GenerateSW({
			swDest: 'sw.js',
			clientsClaim: true,
			skipWaiting: true
		}),
		new CopyPlugin([
			{ from: 'public', to: '' },
		]),
	],
};
