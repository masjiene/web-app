FROM nginx

MAINTAINER Francois van der Merwe (swyser.co.za)

COPY ../config/web/nginx/config/default.conf /etc/nginx/conf.d/
COPY ../config/web/nginx/certs/nginx.crt /etc/ssl/
COPY ../config/web/nginx/certs/nginx.key /etc/ssl/

# RUN apt-get update && \
# 	apt-get -y install gnupg2 && \
# 	apt-get -y install curl && \
#   curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
# 	apt-get -y install nodejs

# RUN npm install

COPY ./dist /usr/share/nginx/html

EXPOSE 80
EXPOSE 443

# docker build -t swyserapp/web .
# docker run --rm -p 80:80/tcp -p 443:443/tcp --name swyserappweb -dit swyserapp/web
