const merge = require('webpack-merge');
const path = require('path');

const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'source-map',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		watchContentBase: true,
		port: 8080,
		overlay: {
			warnings: true,
			errors: true,
		},
		proxy: {
			'/api': 'http://localhost:8082/api'
		},
		open: true,
		progress: true,
		writeToDisk: true
	},
});
