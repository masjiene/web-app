const webpackDashboard = require('webpack-dashboard/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const merge = require('webpack-merge');

const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'source-map',
	plugins: [
		new webpackDashboard({ port: 3001 }),
		new BundleAnalyzerPlugin({
			analyzerMode: 'server',
			generateStatsFile: true,
			statsOptions: { source: false },
			analyzerPort: 3002,
		}),
	],
});
