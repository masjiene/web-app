export const dataModel = {
	houses: [
		{
			house: {
				userId: {
					required: true,
				},
				details: {
					name: {
						required: true,
					},
					description: {
						required: false,
					},
					location: {
						required: false,
					},
				},
				rooms: [
					{
						name: {
							required: false,
						},
						devices: [
							{
								name: {
									required: false,
								},
								address: {
									required: false,
								},
								state: {
									required: false,
									default: 0,
								},
								command: {
									required: false,
								},
							},
						],
					},
				],
				devices: [
					{
						name: {
							required: false,
						},
						address: {
							required: false,
						},
						state: {
							required: false,
							default: 0,
						},
						command: {
							required: false,
						},
					},
				],
			},
		},
	],
};
