import { jsonToGraphQLQuery } from 'json-to-graphql-query';


let headers = new Headers();
const baseUrl = './api';

headers.append('Content-Type', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST');
headers.append('Access-Control-Allow-Credentials', 'true');

// House api
export async function getHouse() {
	const query = {
		query: {
			house: {
				_id: true,
				details: {
					name: true,
					description: true,
					location: {
						lat: true,
						long: true,
					}
				},
				rooms: {
					_id: true,
					name: true,
					description: true,
					devices: {
						_id: true,
						name: true,
						state: true,
						address: true,
						command: true,
						type: true,
						port: true,
					}
				}
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}

export async function createHouse(details) {
	const query = {
		mutation: {
			createHouse: {
				__args: {
					details: details,
				},
				_id: true,
				details: {
					name: true,
					description: true,
					location: {
						lat: true,
						long: true,
					}
				},
				rooms: {
					_id: true,
					name: true,
					description: true,
					devices: {
						_id: true,
						name: true,
						state: true,
						address: true,
						command: true,
						type: true,
						port: true,
					}
				}
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}

export async function createRoom(room) {
	const query = {
		mutation: {
			createRoom: {
				__args: {
					name: room.name,
					description: room.description
				},
				_id: true,
				details: {
					name: true,
					description: true,
					location: {
						lat: true,
						long: true,
					}
				},
				rooms: {
					_id: true,
					name: true,
					description: true,
					devices: {
						_id: true,
						name: true,
						state: true,
						address: true,
						command: true,
						type: true,
						port: true,
					}
				}
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}

export async function updateDevice(device) {
	const query = {
		mutation: {
			updateDevice: {
				__args: {
					id: device._id,
					name: device.name,
					state: device.state,
					address: device.address,
					command: device.command,
					port: device.port
				},
				_id: true,
				details: {
					name: true,
					description: true,
					location: {
						lat: true,
						long: true,
					}
				},
				rooms: {
					_id: true,
					name: true,
					description: true,
					devices: {
						_id: true,
						name: true,
						state: true,
						address: true,
						command: true,
						type: true,
						port: true,
					}
				}
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}

// User api
export async function login(email, password) {
	const query = {
		query: {
			login: {
				__args: {
					email: email,
					password: password,
				},
				userId: true,
				token: true,
				tokenExpiration: true,
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}
export async function logout() {
	const query = {
		query: {
			logout: {
				userId: true,
				token: false,
				tokenExpiration: false,
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}

export async function signup(email, password) {
	const query = {
		mutation: {
			signup: {
				__args: {
					email: email,
					password: password,
				},
				userId: true,
			}
		}
	};

	return await fetch(baseUrl, {
		method: 'POST',
		credentials: 'same-origin',
		headers: headers,
		redirect: 'follow',
		body: JSON.stringify({ query: jsonToGraphQLQuery(query)}),
	});
}
