import * as ACTIONS from './actionTypes';
import * as api from './api';

// House ACTIONS
export const getHouse = () => {
	return dispatch => {
		dispatch(getHouseBegin());
		api.getHouse()
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(getHouseSuccess(res.data));
				} else {
					dispatch(getHouseError(res.errors[0]));
				}
			});
	};
};

export const getHouseBegin = () => ({
	type: ACTIONS.GET_HOUSE_BEGIN,
});

export const getHouseSuccess = payload => ({
	type: ACTIONS.GET_HOUSE_SUCCESS,
	payload: payload,
});

export const getHouseError = payload => ({
	type: ACTIONS.GET_HOUSE_ERROR,
	payload: payload,
});

export const updateHouseFromWebsocket = payload => ({
	type: ACTIONS.UPDATE_HOUSE_FROM_WEBSOCKET,
	payload: payload,
});


export const createHouse = (details) => {
	return dispatch => {
		dispatch(createHouseBegin());
		api.createHouse(details)
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(createHouseSuccess(res.data));
				} else {
					dispatch(createHouseError(res.errors[0]));
				}
			});
	};
};

export const createHouseBegin = () => ({
	type: ACTIONS.CREATE_HOUSE_BEGIN,
});

export const createHouseSuccess = payload => ({
	type: ACTIONS.CREATE_HOUSE_SUCCESS,
	payload: payload,
});

export const createHouseError = payload => ({
	type: ACTIONS.CREATE_HOUSE_ERROR,
	payload: payload,
});

export const createRoom = (room) => {
	return dispatch => {
		dispatch(createRoomBegin());
		api.createRoom(room)
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(createRoomSuccess(res.data));
				} else {
					dispatch(createRoomError(res.errors[0]));
				}
			});
	};
};

export const createRoomBegin = () => ({
	type: ACTIONS.CREATE_ROOM_BEGIN,
});

export const createRoomSuccess = payload => ({
	type: ACTIONS.CREATE_ROOM_SUCCESS,
	payload: payload,
});

export const createRoomError = payload => ({
	type: ACTIONS.CREATE_ROOM_ERROR,
	payload: payload,
});





// Device ACTIONS
export const updateDevice = (device) => {
	return dispatch => {
		dispatch(updateDeviceBegin());
		api.updateDevice(device)
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(updateDeviceSuccess(res.data.updateDevice));
				} else {
					dispatch(updateDeviceError(res.errors[0]));
				}
			});
	};
};

export const updateDeviceBegin = () => ({
	type: ACTIONS.UPDATE_DEVICE_BEGIN,
});

export const updateDeviceSuccess = payload => ({
	type: ACTIONS.UPDATE_DEVICE_SUCCESS,
	payload: payload,
});

export const updateDeviceError = payload => ({
	type: ACTIONS.UPDATE_DEVICE_ERROR,
	payload: payload,
});






// User ACTIONS
export const login = (email, password) => {
	return dispatch => {
		dispatch(loginBegin());
		api.login(email, password)
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(loginSuccess(res.data));
				} else {
					dispatch(loginError(res.errors[0]));
				}
			});
	};
};

export const loginCheck = () => ({
	type: ACTIONS.LOGIN_CHECK,
});

export const loginBegin = () => ({
	type: ACTIONS.LOGIN_BEGIN,
});

export const loginSuccess = payload => ({
	type: ACTIONS.LOGIN_SUCCESS,
	payload: payload,
});

export const loginError = payload => ({
	type: ACTIONS.LOGIN_ERROR,
	payload: payload,
});

export const signup = (email, password, rePassword) => {
	return dispatch => {
		dispatch(signupBegin());
		if (password !== rePassword) {
			return signupError(new Error('passwords do not match!'));
		}
		api.signup(email, password)
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(signupSuccess(res.data));
				} else {
					dispatch(signupError(res.errors[0]));
				}
			});
	};
};

export const signupBegin = () => ({
	type: ACTIONS.SIGNUP_BEGIN,
});

export const signupSuccess = payload => ({
	type: ACTIONS.SIGNUP_SUCCESS,
	payload: payload,
});

export const signupError = payload => ({
	type: ACTIONS.SIGNUP_ERROR,
	payload: payload,
});

export const logout = () => {
	return dispatch => {
		dispatch(logoutBegin());
		api.logout()
			.then(res => {
				return res.json();
			})
			.then(res => {
				if (!res.errors) {
					dispatch(logoutSuccess(res.data));
				} else {
					dispatch(logoutError(res.errors[0]));
				}
			});
	};
};

export const logoutBegin = () => ({
	type: ACTIONS.LOGOUT_BEGIN,
});

export const logoutSuccess = payload => ({
	type: ACTIONS.LOGOUT_SUCCESS,
	payload: payload,
});

export const logoutError = payload => ({
	type: ACTIONS.LOGOUT_ERROR,
	payload: payload,
});

// Application ACTIONS
export const successDone = () => ({
	type: ACTIONS.SUCCESS_DONE
});
export const errorDone = () => ({
	type: ACTIONS.ERROR_DONE
});
