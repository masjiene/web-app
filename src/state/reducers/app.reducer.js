import * as ACTIONS from '../actionTypes';

const initialState = {
	title: 'swyser.app',
	drawer: false,
	view: {
		loading: 0,
		error: null,
		success: false,
		setup: false
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
	case ACTIONS.SUCCESS_DONE:
		return {
			...state,
			view: {
				...state.view,
				success: false
			},
		};
	case ACTIONS.ERROR_DONE:
		return {
			...state,
			view: {
				...state.view,
				error: null
			},
		};
	case ACTIONS.GET_HOUSE_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
				error: null,
			},
		};
	case ACTIONS.GET_HOUSE_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: null,
				setup: action.payload.house ? false : true
			},
		};
	case ACTIONS.GET_HOUSE_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};
	case ACTIONS.CREATE_HOUSE_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
				error: null,
			},
		};
	case ACTIONS.CREATE_HOUSE_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				success: true,
				error: null
			},
		};
	case ACTIONS.CREATE_HOUSE_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};
	case ACTIONS.CREATE_ROOM_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
				error: null,
			},
		};
	case ACTIONS.CREATE_ROOM_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: null
			},
		};
	case ACTIONS.CREATE_ROOM_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};
	case ACTIONS.LOGIN_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
				error: null,
			},
		};
	case ACTIONS.LOGIN_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: null
			},
		};
	case ACTIONS.LOGIN_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};

	case ACTIONS.SIGNUP_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
				error: null,
			},
		};
	case ACTIONS.SIGNUP_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: null,
				success: true
			},
		};
	case ACTIONS.SIGNUP_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};

	case ACTIONS.UPDATE_DEVICE_BEGIN:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading + 1,
			},
		};
	case ACTIONS.UPDATE_DEVICE_SUCCESS:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				success: true
			},
		};
	case ACTIONS.UPDATE_DEVICE_ERROR:
		return {
			...state,
			view: {
				...state.view,
				loading: state.view.loading - 1,
				error: action.payload.message,
			},
		};
	default:
		return state;
	}
};
