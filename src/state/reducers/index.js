import { combineReducers } from 'redux';
import houseReducer from './house.reducer';
import userReducer from './user.reducer';
import appReducer from './app.reducer';

export default combineReducers({
	app: appReducer,
	house: houseReducer,
	user: userReducer
});
