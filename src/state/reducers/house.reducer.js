import * as ACTIONS from '../actionTypes';

const initialState = null;

export default (state = initialState, action) => {
	switch (action.type) {
	case ACTIONS.GET_HOUSE_SUCCESS:
		if(!action.payload.house) {
			return null;
		}

		return {
			...state,
			...action.payload.house,
		};
	case ACTIONS.UPDATE_HOUSE_FROM_WEBSOCKET:
		if(!action.payload.house) {
			return null;
		}

		navigator.vibrate(200);
		return {
			...state,
			...action.payload.house,
		};
	case ACTIONS.CREATE_HOUSE_SUCCESS:
		if(!action.payload) {
			return null;
		}

		return {
			...state,
			...action.payload.createHouse,
		};
	case ACTIONS.CREATE_ROOM_SUCCESS:
		if(!action.payload) {
			return null;
		}

		return {
			...state,
			...action.payload.createRoom,
		};
	case ACTIONS.UPDATE_DEVICE_SUCCESS:
		if(!action.payload) {
			return null;
		}

		navigator.vibrate(200);

		return {
			...state,
			...action.payload,
		};
	default:
		return state;
	}
};
