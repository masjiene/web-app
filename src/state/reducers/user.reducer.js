import * as ACTIONS from '../actionTypes';

const initialState = {};

export default (state = initialState, action) => {
	switch (action.type) {
	case ACTIONS.LOGIN_SUCCESS:
	// TODO: Eventual Consistency sucks - Navigation needs identified
		setTimeout(() => {
			window.location.href = '/#';
		}, 100);

		return {
			...state,
			...action.payload.login,
		};
	case ACTIONS.LOGIN_ERROR:
		setTimeout(() => {
			window.location.href = '/#login';
		}, 100);

		return {
			...state,
			error: action.payload.message,
		};
	case ACTIONS.LOGIN_CHECK:
		return {
			...state,
		};
	case ACTIONS.LOGOUT_SUCCESS:
		// TODO: Eventual Consistency sucks - Navigation needs identified
		setTimeout(() => {
			location.reload();
		}, 100);

		return {
			...state,
			...action.payload.logout,
		};
	default:
		return state;
	}
};
