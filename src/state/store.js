import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
/* eslint-disable */

const middleWare = () => {
	if (process.env.NODE_ENV === 'development') {
		const loggerMiddleware = createLogger();
		return applyMiddleware(thunkMiddleware, loggerMiddleware);
	}

	return applyMiddleware(thunkMiddleware);
};

export default createStore(
	rootReducer,
	middleWare(),
	// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
