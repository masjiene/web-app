import store from './state/store';
import { connect } from 'pwa-helpers';
import { LitElement, html } from 'lit-element';
import './router';
import '../images/logo.png';

export class SwyserApp extends connect(store)(LitElement) {
	static get properties() {
		return {
			state: { type: Object }
		};
	}

	constructor() {
		super();

		window.location.href = '/#';
	}

	connectedCallback() {
		super.connectedCallback();
		import('./framework/swyser-loader');
		import('./framework/swyser-success');
		import('./framework/swyser-error');
	}

	stateChanged(state) {
		this.state = state.app;
	}

	render() {
		return html`
			<style>
				.main {
					height: 100%;
					width: 100%;
				}
			</style>
			<div class="main">
				<swyser-router></swyser-router>
			</div>
            <swyser-loader .loading="${this.state.view.loading > 0}"></swyser-loader>
            <swyser-success .success="${this.state.view.success}"></swyser-success>
            <swyser-error .error="${!!this.state.view.error}"></swyser-error>
		`;
	}
}

customElements.define('swyser-app', SwyserApp);
