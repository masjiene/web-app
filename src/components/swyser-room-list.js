import { LitElement, html } from 'lit-element';
import '../components/swyser-room';
import '../components/swyser-create-room';

class SwyserRoomList extends LitElement {
	static get properties() {
		return {
			house: { type: Object }
		};
	}

	constructor() {
		super();
	}

	render() {
		return html`
			<style>
				:host {
					display: contents;
				}
				main {
					display: flex;
					flex-direction: column;
					flex: 1;
					overflow: auto;
				}
				section {
					height: 100%;
					overflow: auto;
				}

				@media only screen and (max-width: 1024px) {
					section {
						scroll-snap-type: x mandatory;
						display: flex;
						overflow-x: scroll;
						height: 100%;
						width: 100%;
					}

					section * {
						min-width: 100vw;
						min-height: 100vw;
						scroll-snap-align: center;
					}
				}
				@media only screen and (min-width: 1025px) {
					section {
						display: grid;
						grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
						grid-gap: 10px;
						grid-auto-rows: minmax(auto-fill, auto);
						grid-auto-flow: dense;
					}

					section * {
						color: #fff;
					}
				}
			</style>
			<main>
				<section>
					${this.house ? this.house.rooms.map((room) => html`<swyser-room .room="${room}"></swyser-room>`) : ''}
					<swyser-create-room></swyser-create-room>
				</section>
			</main>
		`;
	}
}

customElements.define('swyser-room-list', SwyserRoomList);
