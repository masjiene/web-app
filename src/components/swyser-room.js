import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import '../components/swyser-device';
import { plus } from 'octicons';

class SwyserRoom extends LitElement {
	static get properties() {
		return {
			room: { type: Object }
		};
	}

	constructor() {
		super();
	}

	// connectedCallback() {
	// 	this.shadowRoot.querySelector('.title').innerHTML = this.data.name;
	// 	this.shadowRoot.querySelector('.icon').addEventListener('click', this.addDevice.bind(null, this), false);

	// 	let devicesElement = this.shadowRoot.querySelector(
	// 		'.device'
	// 	);

	// 	if (this.data.devices.length > 0) {
	// 		this.data.devices.forEach(device => {
	// 			let newDevice = document.createElement('swyser-device');
	// 			newDevice.data = device;
	// 			devicesElement.appendChild(newDevice);
	// 		});
	// 	} else {
	// 		devicesElement.innerHTML = 'No devices!!!';
	// 	}
	// }

	render() {
		return html`
			<style>
				.main {
					margin: 15px;
					border: solid 1px #eff1f3;
					border-radius: 5px;
				}
				.header {
					display: flex;
					padding: 8px 8px 8px 16px;
					border-bottom: solid 1px #eff1f3;
					background-color: #272727;
					border-radius: 5px;
				}
				.title {
					display: flex;
					align-items: center;
					font-size: 18px;
				}
				svg {
					height: 40px;
					width: 20px;
				}
				.icon {
					border: solid 1px #fed766;
					display: flex;
					border-radius: 15%;
					width: 35px;
					height: 35px;
					background-color: #272727;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					transition: 0.2s;
					fill: #eff1f3;
				}
				.icon:hover {
					background-color: #fed766;
					border: solid 1px #272727;
					fill: #272727;
				}
				.icon:active {
					background-color: #272727;
					border: solid 1px #fed766;
					transition: 0.2s;
					fill: #fed766;
				}
				.device {
					padding: 15px;
				}
			</style>
			<div class="main">
				<div class="header">
					<div class="title" style="flex-grow: 1;">
						${this.room.name}
					</div>
					<div class="icon" @click="${this.addDevice}">
						${unsafeHTML(plus.toSVG({ 'class': 'svg' }))}
					</div>
				</div>
				<div class="device">
					${this.room.devices ? this.room.devices.map((device) => html`<swyser-device .device="${device}"></swyser-device>`) : ''}
				</div>
			</div>
		`;
	}

	addDevice() {
		console.log('show device list!');
	}
}

customElements.define('swyser-room', SwyserRoom);
