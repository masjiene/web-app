import store from '../state/store';
import { createHouse } from '../state/actions';
import { LitElement, html } from 'lit-element';

class SwyserCreateHouse extends LitElement {
	static get properties() {
		return {
			show: { type: Boolean },
			name: { type: String },
			description: { type: String }
		};
	}

	constructor() {
		super();

		this.show = false;
		this.name = '';
		this.description = '';
	}

	render() {
		return html`
			<style>
				:host {
					display: contents;
				}
				.wrapper {
					height: 100%;
				}
				.hidden {
					display: none !important;
				}
				.show {
					display: contents !important;
				}
				.main {
					margin: 15px;
				}
				.header {
					display: flex;
					padding: 16px 16px 0px 16px;
					border: solid 1px #eff1f3;
					background-color: #272727;
					margin-bottom: 16px;
					border-radius: 5px;
				}
				.title {
					font-size: 24px;
					text-align: center;
				}
				.title > p {
					font-size: 14px;
				}
				.header-svg {
					height: 40px;
					width: 20px;
				}
				.form {
					border: solid 1px #eff1f3;
					border-radius: 5px;
				}
				.form-title {
					font-size: 18px;
					padding: 12px 16px 0px 16px;
				}
				.input {
					box-sizing: border-box;
					width: 100%;
					padding: 0px 15px;
					margin: 10px 0px;
				}
				.input input {
					box-sizing: border-box;
					padding: 20px 10px;
					width: 100%;
					font-size: 15px;
					height: 25px;
					border: solid 1px #272727;
				}
				.button {
					text-align: end;
					flex-grow: 1;
					padding: 0px 15px;
					margin-bottom: 15px;
				}
				.button button {
					background-color: #272727;
					color: #eff1f3;
					border-radius: 5px;
					height: 40px;
					width: 90px;
					cursor: pointer;
					font-size: 14px;
				}
				.button button:hover {
					background-color: #fed766;
					color: #272727;
					font-weight: bold;
				}
				.button button:active {
					background-color: #272727;
					color: #fed766;
				}
				.footer {
					display: flex;
					width: 100%;
					justify-content: center;
					align-items: center;
				}
			</style>
			<div class="wrapper ${this.show ? 'show' : 'hidden'}">
				<div class="main">
					<div class="header">
						<div class="title" style="flex-grow: 1;">
							Welcome!
							<p>New around here?</p>
							<p>Let's get you started, please enter a name and maybe a description for your house:</p>
						</div>
					</div>
					<div class="form">
						<div class="form-title">
							Setup
						</div>
						<div class="input">
							<input value=${this.name} text="house" name="house" type="text" aria-label="house" placeholder="name" @change=${e => this.name = e.target.value}  />
						</div>
						<div class="input">
							<input value=${this.description} name="description" type="text" aria-label="description" placeholder="description" @change=${e => this.description = e.target.value}  />
						</div>
						<div class="footer">
							<div class="button">
								<button @click=${this.createHouse} type="button">Create</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		`;
	}

	createHouse() {
		store.dispatch(createHouse({ name: this.name, description: this.description }));
	}
}

customElements.define('swyser-create-house', SwyserCreateHouse);
