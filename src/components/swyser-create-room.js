import store from '../state/store';
import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { createRoom } from '../state/actions';
import { plus, grabber } from 'octicons';

class SwyserCreateRoom extends LitElement {
	static get properties() {
		return {
			name: { type: String },
			description: { type: String },
			show: { type: Boolean }
		};
	}

	constructor() {
		super();

		this.name = '';
		this.description = '';
		this.show = false;
	}

	render() {
		return html`
			<style>
				.main {
					margin: 15px;
					border: solid 1px #eff1f3;
					display: none;
					flex-direction: column;
					border-radius: 5px;
				}
				.header {
					display: flex;
					padding: 8px 8px 8px 16px;
					border-bottom: solid 1px #eff1f3;
					background-color: #272727;
					border-radius: 5px;
				}
				.title {
					display: flex;
					align-items: center;
					font-size: 18px;
				}
				svg {
					height: 40px;
					width: 20px;
				}
				.hide {
					display: none !important;
				}
				.show {
					display: flex !important;
				}
				.add-room {
					flex-direction: column;
					height: 100%;
					display: flex;
					justify-content: center;
					align-items: center;
				}
				.add-svg {
					height: 45px;
					width: 35px;
				}
				.add-icon {
					border: solid 1px #fed766;
					display: flex;
					border-radius: 15%;
					width: 70px;
					height: 70px;
					background-color: #272727;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					transition: 0.2s;
					fill: #eff1f3;
				}
				.add-icon:hover {
					background-color: #fed766;
					border: solid 1px #272727;
					fill: #272727;
				}
				.add-icon:active {
					background-color: #272727;
					border: solid 1px #fed766;
					transition: 0.2s;
					fill: #fed766;
				}
				.icon {
					border: solid 1px #fed766;
					display: flex;
					border-radius: 15%;
					width: 35px;
					height: 35px;
					background-color: #272727;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					transition: 0.2s;
					fill: #eff1f3;
				}
				.icon:hover {
					background-color: #fed766;
					border: solid 1px #272727;
					fill: #272727;
				}
				.icon:active {
					background-color: #272727;
					border: solid 1px #fed766;
					transition: 0.2s;
					fill: #fed766;
				}
				.input {
					box-sizing: border-box;
					width: 100%;
					padding: 0px 15px;
					margin: 10px 0px;
				}
				.input input {
					box-sizing: border-box;
					padding: 20px 10px;
					width: 100%;
					font-size: 15px;
					height: 25px;
					border: solid 1px #272727;
				}
				.button {
					text-align: end;
					padding: 0px 15px;
					margin-bottom: 15px;
				}
				.button button {
					background-color: #272727;
					color: #eff1f3;
					border-radius: 5px;
					height: 40px;
					width: 90px;
					cursor: pointer;
					font-size: 14px;
				}
				.button button:hover {
					background-color: #fed766;
					color: #272727;
					font-weight: bold;
				}
				.button button:active {
					background-color: #272727;
					color: #fed766;
				}
				.footer {
					display: flex;
					width: 100%;
				}
			</style>
			<div class="add-room ${!this.show ? 'show' : 'hide'}">
				<div class="add-icon" @click="${this.toggle}">
					${unsafeHTML(plus.toSVG({ 'class': 'svg' }))}
				</div>
			</div>
			<div class="main ${!this.show ? 'hide' : 'show'}">
				<div class="header">
					<div class="title" style="flex-grow: 1;">
						Add new room
					</div>
					<div class="icon">
						${unsafeHTML(grabber.toSVG({ 'class': 'svg' }))}
					</div>
				</div>
				<div class="form">
					<div class="input">
						<input .value="${this.name}" text="name" name="name" type="text" aria-label="name" placeholder="name" @change=${e => this.name = e.target.value} />
					</div>
					<div class="input">
						<input .value="${this.description}" name="description" type="text" aria-label="description" placeholder="description" @change=${e => this.description = e.target.value} />
					</div>
					<div class="footer">
						<div class="button" style="flex-grow: 1;">
							<button @click="${this.toggle}" type="button">Cancel</button>
						</div>
						<div class="button">
							<button @click="${this.createRoom}" type="button">Add</button>
						</div>
					</div>
				</div>
			</div>
		`;
	}

	createRoom() {
		store.dispatch(createRoom({ name: this.name, description: this.description }));
		this.show = false;
		this.name = '';
		this.description = '';
	}

	toggle() {
		this.show = !this.show;
		this.name = '';
		this.description = '';
	}
}

customElements.define('swyser-create-room', SwyserCreateRoom);
