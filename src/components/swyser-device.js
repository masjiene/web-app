import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import store from '../state/store';
import { updateDevice } from '../state/actions';
import * as octicons from 'octicons';

class SwyserDevice extends LitElement {
	static get properties() {
		return {
			device: { type: Object },
			deviceTypes: { type: Array }
		};
	}

	constructor() {
		super();

		this.deviceTypes = [
			{ type: 'light', icon: octicons['light-bulb'] },
			{ type: 'lock', icon: octicons['key'] },
			{ type: 'speaker', icon: octicons['unmute'] },
			{ type: 'window', icon: octicons['inbox'] },
			{ type: 'heating', icon: octicons['broadcast'] },
		];
	}

	// connectedCallback() {
	// if (this.data) {
	// 	let deviceElement = this.shadowRoot.querySelector('#deviceElement');

	// 	const icon = this.deviceTypes.find(type => {
	// 		return type.type === this.data.type;
	// 	});

	// 	let newDevice = document.createElement('div');
	// 	newDevice.addEventListener('click', this.toggleDevice.bind(null, this.data), false);
	// 	newDevice.innerHTML = `
	// <div class="device">
	// 	<div class="icon">
	// 		${icon.icon.toSVG({ class: 'svg' })}
	// 	</div>
	// 	<div class="title">
	// 		${this.data.name}
	// 	</div>
	// 	<div class="state">
	// 		${this.data.state}
	// 	<div>
	// </div>
	// 	`;

	// 	deviceElement.appendChild(newDevice);
	// }
	// }

	render() {
		return html`
			<style>
				.device {
					background-color: #272727;
					color: #eff1f3;
					display: flex;
					align-items: center;
					padding: 10px 15px;
					border: solid 1px #eff1f3;
					margin-bottom: 10px;
					cursor: pointer;
					transition: 0.2s;
					fill: #fed766;
					border-radius: 5px;
				}
				.device:hover {
					background-color: #fed766;
					color: #272727;
					fill: #272727;
				}
				.device:active {
					background-color: #272727;
					color: #eff1f3;
					fill: #eff1f3;
				}
				.icon {
					padding-top: 3px;
				}
				.title {
					font-size: 14px;
					flex-grow: 1;
					padding: 5px 10px;
				}
				.state {
					font-size: 14px;
				}
				svg {
					height: 20px;
					width: 20px;
				}
				@media only screen and (max-width: 1025px) {
					.device:hover {
						background-color: #272727;
						color: #eff1f3;
						fill: #fed766;
					}
				}
			</style>
			<div id="deviceElement">
				<div class="device" @click="${this.toggleDevice}">
					<div class="icon">
						${unsafeHTML(this.deviceTypes.find(type => type.type === this.device.type).icon.toSVG())}
					</div>
					<div class="title">
						${this.device.name}
					</div>
					<div class="state">
						${this.device.state}
					<div>
				</div>
			</div>
		`;
	}

	toggleDevice() {
		console.log('device tapped', this.device);
		store.dispatch(updateDevice({
			...this.device,
			state: this.device.state == 0 ? 1 : 0
		}));
	}
}

customElements.define('swyser-device', SwyserDevice);
