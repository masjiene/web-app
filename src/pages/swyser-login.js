import store from '../state/store';
import { login } from '../state/actions';
import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { home } from 'octicons';

class SwyserLogin extends LitElement {
	static get properties() {
		return {
			username: { type: String },
			password: { type: String }
		};
	}

	constructor() {
		super();
		this.username = '';
		this.password = '';
	}

	render() {
		return html`
			<style>
				:host {
					display: flex;
					width: 100%;
					justify-content: center;
				}
				.main {
					display: flex;
					flex-direction: column;
					height: 70%;
					justify-content: center;
					align-items: center;
					color: #eff1f3;
					width: 35%;
					margin-top: 10%;
				}
				.logo {
					display: flex;
					justify-content: center;
					align-items: center;
					padding-left: 15px;
					margin-bottom: 50px;
				}
				.icon {
					display: flex;
					border-radius: 15%;
					width: 40px;
					height: 40px;
					background-color: #272727;
					border: solid 1px #fed766;
					justify-content: center;
					align-items: center;
					fill: #eff1f3;
				}
				svg {
					height: 40px;
					width: 20px;
					cursor: pointer;
				}
				.title {
					color: #fed766;
					font-size: 32px;
					padding-left: 10px;
					padding-bottom: 7px;
				}
				.input {
					width: 100%;
					margin: 10px 0px;
				}
				.input input {
					box-sizing: border-box;
					padding: 20px 10px;
					width: 100%;
					font-size: 15px;
					height: 25px;
					border: solid 1px #272727;
				}
				.button {
					text-align: end;
					width: 35%;
				}
				.button button {
					background-color: #272727;
					color: #eff1f3;
					border-radius: 5px;
					height: 40px;
					width: 90px;
					cursor: pointer;
					font-size: 16px;
				}
				.button button:hover {
					background-color: #fed766;
					color: #272727;
					font-weight: bold;
				}
				.button button:active {
					background-color: #272727;
					color: #fed766;
				}
				.signup-container {
					flex-grow: 1;
				}
				.signup {
					color: #fed766;
					cursor: pointer;
				}
				.signup:hover {
					color: #eff1f3;
				}
				.footer {
					display: flex;
					width: 100%;
					justify-content: center;
					align-items: center;
				}

				@media only screen and (max-width: 1024px) {
					.main {
						display: flex;
						flex-direction: column;
						height: 70%;
						justify-content: center;
						align-items: center;
						color: #eff1f3;
						width: 100%;
					}
					.input {
						width: 85%;
					}
					.input input {
						width: 100%;
						font-size: 15px;
						height: 30px;
					}
					.button {
						align-items: right;
						background-color: #272727;
						color: #eff1f3;
						border-radius: 5px;
						height: 40px;
						width: 90px;
						cursor: pointer;
						font-size: 16px;
					}
					.signup-container {
						flex-grow: 1;
					}
					.signup-text {
						display: none;
					}
					.footer {
						display: flex;
						width: 85%;
						justify-content: center;
						align-items: center;
					}
				}
			</style>

			<div class="main">
				<div class="logo">
					<span class="icon">
						${unsafeHTML(home.toSVG({ 'class': 'svg' }))}
					</span>
					<span class="title">swyser.app</span>
				</div>
				<div class="input">
					<input .value="${this.username}" text="email" name="email" type="text" aria-label="Email" placeholder="example@domain.com" @change=${e => this.username = e.target.value} />
				</div>
				<div class="input">
					<input .value="${this.password}" name="password" type="password" aria-label="Password" placeholder="password" @change=${e => this.password = e.target.value} @keyup=${e => e.key === 'Enter' ? this.login() : null} />
				</div>
				<div class="footer">
					<div class="signup-container">
						<span class="signup-text">Don't have an account?</span>&nbsp;<span class="signup" @click=${() => window.location.hash = 'signup'}>Signup here</span>
					</div>
					<div class="button">
						<button @click=${this.login} type="button">Login</button>
					</div>
				</div>
			<div>
		`;
	}

	login() {
		store.dispatch(login(this.username, this.password));
		this.username = '';
		this.password = '';
	}
}

customElements.define('swyser-login', SwyserLogin);
