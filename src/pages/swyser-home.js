import store from '../state/store';
import { getHouse } from '../state/actions';
import { connect } from 'pwa-helpers';
import { LitElement, html } from 'lit-element';
import '../components/swyser-room';
import '../framework/swyser-header';
import '../framework/swyser-footer';
import '../components/swyser-room-list';
import '../components/swyser-create-house';
import '../../images/rex.png';

class SwyserHome extends connect(store)(LitElement) {
	static get properties() {
		return {
			state: { type: Object },
			setup: { type: Boolean }
		};
	}

	constructor() {
		super();
		store.dispatch(getHouse());

		this.state = {};
		this.setup = false;
	}

	stateChanged(state) {
		this.state = state;
		this.setup = state.app.view.setup;
	}

	render() {
		return html`
			<style>
				.main {
					display: flex;
					flex-direction: column;
					height: 100%;
					flex: 1;
					overflow: auto;
					background-image: url("./rex.png");
					background-repeat: no-repeat;
					background-attachment: fixed;
					background-position: top right;
					background-size: auto;
				}

				swyser-room-list {
					height: 100%;
				}

				.hidden {
					display: none !important;
				}

				@media only screen and (max-width: 1024px) {
					.main {
						display: flex;
						flex-direction: column;
						height: 100%;
						flex: 1;
						overflow: auto;
						background-image: none;
					}
				}
			</style>
			<div class="main">
				<swyser-header></swyser-header>
				<swyser-create-house .show="${this.setup}"></swyser-create-house>
				<swyser-room-list .house="${this.state.house}"></swyser-room-list>
				<swyser-footer></swyser-footer>
			</div>
		`;
	}
}

customElements.define('swyser-home', SwyserHome);
