import { LitElement, html } from 'lit-element';

class SwyserAbout extends LitElement {
	constructor() {
		super();
	}

	connectedCallback() {}

	render() {
		return html`
			<div>Error!</div>
		`;
	}

	disconnectedCallback() {}
}

customElements.define('swyser-about', SwyserAbout);
