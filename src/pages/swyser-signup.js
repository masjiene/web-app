import store from '../state/store';
import { signup } from '../state/actions';
import { LitElement, html } from 'lit-element';
// import { home } from 'octicons';

class SwyserSignup extends LitElement {
	static get properties() {
		return {
			username: { type: String },
			password: { type: String },
			repassword: { type: String },
		};
	}

	constructor() {
		super();

		this.username = '';
		this.password = '';
		this.repassword = '';
	}

	connectedCallback() {
		super.connectedCallback();
	}

	render() {
		return html`
			<style>
				:host {
					display: flex;
					width: 100%;
					justify-content: center;
				}
				.main {
					display: flex;
					flex-direction: column;
					height: 70%;
					justify-content: center;
					align-items: center;
					color: #eff1f3;
					width: 35%;
					margin-top: 10%;
				}
				.logo {
					display: flex;
					justify-content: center;
					align-items: center;
					padding-left: 15px;
					margin-bottom: 50px;
				}
				.icon {
					display: flex;
					border-radius: 15%;
					width: 40px;
					height: 40px;
					background-color: #272727;
					border: solid 1px #fed766;
					justify-content: center;
					align-items: center;
					fill: #eff1f3;
				}
				svg {
					height: 40px;
					width: 20px;
					cursor: pointer;
				}
				.title {
					color: #fed766;
					font-size: 32px;
					padding-left: 10px;
					padding-bottom: 7px;
				}
				.input {
					width: 100%;
					margin: 10px 0px;
				}
				.input input {
					box-sizing: border-box;
					padding: 20px 10px;
					width: 100%;
					font-size: 15px;
					height: 25px;
					border: solid 1px #272727;
				}
				.button {
					text-align: end;
					width: 35%;
				}
				.button button {
					background-color: #272727;
					color: #eff1f3;
					border-radius: 5px;
					height: 40px;
					width: 90px;
					cursor: pointer;
					font-size: 16px;
				}
				.button button:hover {
					background-color: #fed766;
					color: #272727;
					font-weight: bold;
				}
				.button button:active {
					background-color: #272727;
					color: #fed766;
				}
				.login-container {
					flex-grow: 1;
				}
				.login {
					color: #fed766;
					cursor: pointer;
				}
				.login:hover {
					color: #eff1f3;
				}
				.footer {
					display: flex;
					width: 100%;
					justify-content: center;
					align-items: center;
				}

				@media only screen and (max-width: 1024px) {
					.main {
						display: flex;
						flex-direction: column;
						height: 70%;
						justify-content: center;
						align-items: center;
						color: #eff1f3;
						width: 100%;
					}
					.input {
						width: 85%;
					}
					.input input {
						width: 100%;
						font-size: 15px;
						height: 30px;
					}
					.button {
						align-items: right;
						background-color: #272727;
						color: #eff1f3;
						border-radius: 5px;
						height: 40px;
						width: 90px;
						cursor: pointer;
						font-size: 16px;
					}
					.login-container {
						flex-grow: 1;
					}
					.login-text {
						display: none;
					}
					.footer {
						display: flex;
						width: 85%;
						justify-content: center;
						align-items: center;
					}
				}
			</style>

			<div class="main">
				<div class="logo">
					<span class="icon">
					</span>
					<span class="title">swyser.app</span>
				</div>
				<div class="input" style="font-size: 20px;">
					Signup:
				</div>
				<div class="input">
					<input value="${this.username}" name="email" text="email" type="text" aria-label="Email" placeholder="example@domain.com" @change=${e => this.username = e.target.value} />
				</div>
				<div class="input">
					<input value="${this.password}" name="password" type="password" aria-label="Password" placeholder="password" @change=${e => this.password = e.target.value} />
				</div>
				<div class="input">
					<input value="${this.repassword}" name="password" type="password" aria-label="Password" placeholder="retype password" @change=${e => this.repassword = e.target.value} @keyup=${e => e.key === 'Enter' ? this.login() : null} />
				</div>
				<div class="footer">
					<div class="login-container">
						<span class="login-text">Already have an account?</span>&nbsp;<span class="login" @click=${() => window.location.hash = 'login'}>Login here</span>
					</div>
					<div class="button">
						<button type="button" @click=${this.signup}>Signup</button>
					</div>
				</div>
			<div>
		`;
	}

	signup() {
		store.dispatch(signup(this.username, this.password, this.repassword));
		this.username = '';
		this.password = '';
		this.repassword = '';
		window.location.hash = 'login';
	}

	disconnectedCallback() {}
}

customElements.define('swyser-signup', SwyserSignup);
