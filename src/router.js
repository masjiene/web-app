import store from './state/store';
import { loginCheck } from './state/actions';
import './pages/swyser-home';

class SwyserRouter extends HTMLElement {
	constructor() {
		super();

		this.routes = [
			{
				title: 'Home',
				name: '<swyser-home></swyser-home>',
				path: '/',
				url: './pages/swyser-home',
				protected: true,
			},
			{
				title: 'Login',
				name: '<swyser-login></swyser-login>',
				path: 'login',
				url: './pages/swyser-login',
				protected: false,
			},
			{
				title: 'signup',
				name: '<swyser-signup></swyser-signup>',
				path: 'signup',
				url: './pages/swyser-signup',
				protected: false,
			},
			{
				title: 'About',
				name: '<swyser-about></swyser-about>',
				path: 'about',
				url: './pages/swyser-about',
				protected: false,
			},
			{
				title: 'Http404',
				name: '<swyser-http404></swyser-http404>',
				path: 'http404',
				url: './framework/swyser-http404',
				protected: false,
			},
		];
	}

	getCookie(cookie) {
		let v = document.cookie.match('(^|;) ?' + cookie + '=([^;]*)(;|$)');
		return v ? true : false;
	}

	changeRoute() {
		store.dispatch(loginCheck());
		let url = window.location.hash.slice(1) || '/';

		let route = this.routes.find(i => i.path === url);

		if (!route) {
			route = this.routes.find(i => i.path === 'http404');
			window.location.hash = 'http404';
		} else {
			if (route.protected && !this.getCookie('user')) {
				route = this.routes.find(i => i.path === 'login');
				window.location.hash = 'login';
			}
		}

		import(`${route.url}`).then(() => {
			this.innerHTML = `${route.name}`;
		});
	}

	connectedCallback() {
		window.onpopstate = () => {
			this.changeRoute();
		};
	}

	disconnectedCallback() {}
}

customElements.define('swyser-router', SwyserRouter);
