import { LitElement, html } from 'lit-element';

class SwyserDrawer extends LitElement {
	constructor() {
		super();
	}

	connectedCallback() {}

	render() {
		return html`
			<div>Error!</div>
		`;
	}

	disconnectedCallback() {}
}

customElements.define('swyser-drawer', SwyserDrawer);
