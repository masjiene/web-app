import { LitElement, html } from 'lit-element';

class SwyserHttp404 extends LitElement {
	constructor() {
		super();
	}

	connectedCallback() {}

	render() {
		return html`
			<div>Error!</div>
		`;
	}

	disconnectedCallback() {}
}

customElements.define('swyser-http404', SwyserHttp404);
