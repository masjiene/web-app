// eslint-disable-next-line
import { LitElement, html } from 'lit-element';

class SwyserFooterItem extends LitElement {
	constructor() {
		super();
	}

	render() {
		return html`
			<div>Error!</div>
		`;
	}

	connectedCallback() {}
}

customElements.define('swyser-footer-item', SwyserFooterItem);
