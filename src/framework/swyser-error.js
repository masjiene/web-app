import store from '../state/store';
import { LitElement, html } from 'lit-element';
import { errorDone } from '../state/actions';

class SwyserError extends LitElement {
	static get properties() {
		return {
			error: { type: Boolean }
		};
	}

	constructor() {
		super();
	}

	render() {
		return html`
			<style>
				.error {
					position: fixed;
					bottom: 0;
					height: 0px;
					width: 100%;
					background-color: #272727;

					-webkit-transition: height 0.3s ease-in-out;
					-moz-transition: height 0.3s ease-in-out;
					-ms-transition: height 0.3s ease-in-out;
					-o-transition: height 0.3s ease-in-out;

					text-align: center;
				}
				.show {
					height: 64px;
				}
			</style>
			<div class="error ${this.error ? 'show' : ''}">
				<div style="border-top: solid 4px #c60000;"></div>
				<div style="margin-top:17px;font-size: 20px;">
					Eeeeish...broken!
				<d/iv>
			</div>
		`;
	}

	updated() {
		if(this.error) {
			setTimeout(() => {
				this.error = false;
				store.dispatch(errorDone());
			}, 2000);
		}
	}
}

customElements.define('swyser-error', SwyserError);
