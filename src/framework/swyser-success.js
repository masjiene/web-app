import store from '../state/store';
import { LitElement, html } from 'lit-element';
import { successDone } from '../state/actions';

class SwyserSuccess extends LitElement {
	static get properties() {
		return {
			success: { type: Boolean }
		};
	}

	constructor() {
		super();
	}

	render() {
		return html`
			<style>
				.success {
					position: fixed;
					bottom: 0;
					height: 0px;
					width: 100%;
					background-color: #272727;

					-webkit-transition: height 0.2s ease-in-out;
					-moz-transition: height 0.2s ease-in-out;
					-ms-transition: height 0.2s ease-in-out;
					-o-transition: height 0.2s ease-in-out;

					text-align: center;
				}
				.show {
					height: 64px;
				}
			</style>
			<div class="success ${this.success ? 'show' : ''}">
				<div style="border-top: solid 4px #128e1a;"></div>
				<div style="font-size: 20px;margin-top: 17px;">
					done...
				</div>
			</div>
		`;
	}

	updated() {
		if(this.success) {
			setTimeout(() => {
				store.dispatch(successDone());
			}, 1500);
		}
	}
}

customElements.define('swyser-success', SwyserSuccess);
