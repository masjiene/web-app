import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { grabber, mail, bell, gear } from 'octicons';

class SwyserFooter extends LitElement {
	constructor() {
		super();
	}

	render() {
		return html`
			<style>
				.main {
					display: flex;
					flex-grow: 1;
					justify-content: center;
					align-items: center;
					background-color: #272727;
					height: 60px;
					width: 100%;
					color: #272727;
					border-top: solid 3px #fed766;
				}
				.main div {
					display: flex;
					height: 100%;
					flex-grow: 1;
					justify-content: center;
					align-items: center;
				}
				svg {
					height: 40px;
					width: 20px;
				}
				.icon {
					border: solid 1px #fed766;
					display: flex;
					border-radius: 15%;
					width: 45px;
					height: 45px;
					background-color: #272727;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					transition: 0.2s;
					fill: #eff1f3;
				}
				.icon:hover {
					background-color: #fed766;
					border: solid 1px #272727;
					fill: #272727;
				}
				.icon:active {
					background-color: #272727;
					border: solid 1px #fed766;
					transition: 0.2s;
					fill: #fed766;
				}
			</style>
			<footer class="main">
				<div><span class="icon">${unsafeHTML(grabber.toSVG({ 'class': 'svg' }))}</span></div>
				<div><span class="icon">${unsafeHTML(mail.toSVG({ 'class': 'svg' }))}</span></div>
				<div><span class="icon">${unsafeHTML(bell.toSVG({ 'class': 'svg' }))}</span></div>
				<div><span class="icon">${unsafeHTML(gear.toSVG({ 'class': 'svg' }))}</span></div>
			</footer>
		`;
	}
}

customElements.define('swyser-footer', SwyserFooter);
