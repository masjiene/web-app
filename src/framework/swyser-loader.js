import { LitElement, html } from 'lit-element';

class SwyserLoader extends LitElement {
	static get properties() {
		return {
			loading: { type: Boolean },
		};
	}

	constructor() {
		super();
	}

	connectedCallback() {
		super.connectedCallback();
	}

	disconnectedCallback() {}

	render() {
		return html`
			<style>
				.loader {
					position: absolute;
					top:0;
					bottom: 0;
					left: 0;
					right: 0;
					width: 100%;
					height: 100%;
				}
				.hidden {
					display: none;
				}
				.spinner {
					position: absolute;
					top:0;
					bottom: 0;
					left: 0;
					right: 0;
					margin: auto;
					width: 90px;
					height: 40px;
					text-align: center;
					font-size: 10px;
				}

				.spinner > div {
					background-color: #fff;
					height: 100%;
					width: 6px;
					display: inline-block;

					-webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
					animation: sk-stretchdelay 1.2s infinite ease-in-out;
				}

				.spinner .rect2 {
					-webkit-animation-delay: -1.1s;
					animation-delay: -1.1s;
				}

				.spinner .rect3 {
					-webkit-animation-delay: -1.0s;
					animation-delay: -1.0s;
				}

				.spinner .rect4 {
					-webkit-animation-delay: -0.9s;
					animation-delay: -0.9s;
				}

				.spinner .rect5 {
					-webkit-animation-delay: -0.8s;
					animation-delay: -0.8s;
				}

				.spinner .rect6 {
					-webkit-animation-delay: -0.7s;
					animation-delay: -0.7s;
				}

				.spinner .rect7 {
					-webkit-animation-delay: -0.6s;
					animation-delay: -0.6s;
				}

				@-webkit-keyframes sk-stretchdelay {
					0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
					20% { -webkit-transform: scaleY(1.0) }
				}

				@keyframes sk-stretchdelay {
					0%, 40%, 100% {
					transform: scaleY(0.4);
					-webkit-transform: scaleY(0.4);
					}  20% {
					transform: scaleY(1.0);
					-webkit-transform: scaleY(1.0);
					}
				}
			</style>
			<div class="loader ${this.loading ? '' : 'hidden'}">
					<div class="spinner">
						<div class="rect1"></div>
						<div class="rect2"></div>
						<div class="rect3"></div>
						<div class="rect4"></div>
						<div class="rect5"></div>
						<div class="rect6"></div>
						<div class="rect7"></div>
					</div>
			</div>
			`;
	}
}


customElements.define('swyser-loader', SwyserLoader);
