import store from '../state/store';
import { connect } from 'pwa-helpers';
import { logout } from '../state/actions';
import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import * as octicons from 'octicons';

class SwyserHeader extends connect(store)(LitElement) {
	static get properties() {
		return {
			state: { type: Object }
		};
	}

	constructor() {
		super();
	}

	stateChanged(state) {
		this.state = state.app;
	}

	render() {
		return html`
			<style>
				.main {
					display: flex;
					height: 60px;
					width: 100%;
					background-color: #272727;
					color: #fed766;
					border-bottom: solid 3px #fed766;
				}
				.logo {
					display: flex;
					justify-content: center;
					align-items: center;
					padding-left: 15px;
				}
				.logout {
					height: 100%;
					flex-grow: 1;
					width: 90px;
					display: flex;
					justify-content: flex-end;
					align-items: center;
					padding-right: 20px;
					fill: #eff1f3;
				}
				.logout:active {
					fill: #fed766;
					transition: 0.2s;
				}
				svg {
					height: 40px;
					width: 20px;
					cursor: pointer;
				}
				.title {
					color: #fed766;
					font-size: 32px;
					padding-left: 10px;
					padding-bottom: 7px;
				}
				.icon {
					display: flex;
					border-radius: 15%;
					width: 40px;
					height: 40px;
					background-color: #272727;
					border: solid 1px #fed766;
					justify-content: center;
					align-items: center;
					cursor: pointer;
					transition: 0.2s;
					fill: #eff1f3;
				}
				.icon:hover {
					background-color: #fed766;
					fill: #272727;
				}
				.icon:active {
					background-color: #272727;
					border: solid 1px #fed766;
					fill: #fed766;
					transition: 0.2s;
				}
			</style>
			<header class="main">
				<div class="logo">
					<span class="icon">
						${unsafeHTML(octicons.home.toSVG({ 'class': 'svg' }))}
					</span>
					<span class="title">${this.state.title}</span>
				</div>
				<div class="logout" @click=${this.logout}>
					${unsafeHTML(octicons['sign-out'].toSVG({ 'class': 'svg' }))}
				</div>
			</header>
		`;
	}

	logout() {
		store.dispatch(logout());
	}
}

customElements.define('swyser-header', SwyserHeader);
