import { LitElement, html } from 'lit-element';

class SwyserUnauthorized extends LitElement {
	constructor() {
		super();
	}

	connectedCallback() {}

	render() {
		return html`
			<div>Error!</div>
		`;
	}

	disconnectedCallback() {}
}

customElements.define('swyser-unauthorized', SwyserUnauthorized);
