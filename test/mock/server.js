const {
	makeExecutableSchema,
	addMockFunctionsToSchema,
} = require('graphql-tools');
const express = require('express');
const graphqlHttp = require('express-graphql');
const cors = require('cors');

const graphQlSchema = require('./schemas/schema');
const mockResolvers = require('./resolvers/resolver');

const schema = makeExecutableSchema({ typeDefs: graphQlSchema });

const mocks = {
	Query: () => ({
		houses: () => mockResolvers.houses,
		house: () => mockResolvers.house,
		login: () => mockResolvers.login,
		users: () => mockResolvers.users,
	}),
	Mutation: () => ({
		createHouse: () => mockResolvers.createHouse,
		deleteHouse: () => mockResolvers.deleteHouse,
		createUser: () => mockResolvers.createUser,
		deleteUser: () => mockResolvers.deleteUser,
	}),
};

addMockFunctionsToSchema({ schema, mocks });

const app = express();
app.use(cors());

app.use('/graphql', (req, res) => {
	return graphqlHttp({
		schema: schema,
		graphiql: true,
	})(req, res);
});

app.listen(8082);
console.log('mock server is running...');
