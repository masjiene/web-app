module.exports = {
	houses: [
		{
			title: 'house',
			description: 'house 1 description',
			lights: [
				{ title: 'light1', room: '1' },
				{ title: 'light2', room: '2' },
				{ title: 'light3', room: '3' },
			],
		},
	],
	house: {
		title: 'house 2',
		description: 'house 2 description',
		lights: [
			{ title: 'light1', room: '1' },
			{ title: 'light2', room: '2' },
			{ title: 'light3', room: '3' },
		],
	},
	createHouse: {
		title: 'house',
		description: 'house 1 description',
		lights: [
			{ title: 'light1', room: '1' },
			{ title: 'light2', room: '2' },
			{ title: 'light3', room: '3' },
		],
	},
	deleteHouse: {
		_id: '5c332e32dce6b40fd7d31595',
		title: 'house 2',
		description: 'house 2 description',
		lights: [{ title: 'light2', room: '2' }],
	},
	login: {
		userId: '5c332e32dce6b40fd7d31595',
		token:
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YzJmZDQ4ZmY3MjYxYmNhODkxZTkxM2UiLCJlbWFpbCI6InN3eXNlckBsaXZlLmNvLnphIiwiaWF0IjoxNTQ3MDU2MzAzLCJleHAiOjE1NDcwNTk5MDN9.1Cf9PpBItk03-eh7dW3obC-my9Bj8L8WEBrOcUTBFUA',
		tokenExpiration: 1,
	},
	createUser: {
		email: 'email@domain.nl',
		password: null,
	},
	deleteUser: {
		email: 'email@domain.nl',
		password: null,
	},
	users: [
		{
			email: 'email@domain.nl',
			password: null,
		},
		{
			email: 'asdf@domain.nl',
			password: null,
		},
	],
};
