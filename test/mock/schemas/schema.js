module.exports =  `
    type House {
        _id: ID!
        title: String!
        description: String!
        lights: [Light]!
    }

    input HouseInput {
        title: String!
        description: String!
        lights: [LightInput]!
    }

    type Light {
        _id: ID!
        title: String!
        room: String!
    }

    input LightInput {
        title: String!
        room: String!
    }

    type Query {
        house(_id: ID!): House
        houses: [House!]!
        light: [Light]!
        login(email: String!, password: String!): AuthData!
        users: [User]!
    }

    type Mutation {
        createHouse(houseInput: HouseInput): House
        createLight(lightInput: LightInput): Light
        deleteHouse(_id: ID): House
        createUser(userInput: UserInput): User
        deleteUser(_id: ID): User
    }
    type User {
        _id: ID!
        email: String!
        password: String
    }

    type AuthData {
        userId: ID!
        token: String!
        tokenExpiration: Int!
    }

    input UserInput {
        email: String!
        password: String!
    }
`;
